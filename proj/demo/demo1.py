# from .pobj.dev_tools import DevTools
from pobj import pobj, pobjl
import pandas as pd


# pobj = DevTools.pobj
logger = pobjl('demo', 'info')

class Demo1:

    def greet(self):
        return 0

    def get_int(self, int_value: int):
        pobj(int_value)
        logger.info(int_value)

    def get_str(self, str_value: str):
        pobj(str_value)

    def get_bool(self, bool_value: bool):
        pobj(bool_value)

    def get_list(self, list_value: list):
        pobj(list_value)

    def get_dict(self, dict_value: dict):
        pobj(dict_value)

    def get_pandas_dataframe(self, df_value=None):
        fruit_df = pd.DataFrame()
        fruit_df['fruit'] = ['apple', 'banana', 'chikoo', 'dragon-fruit', 'elder-berry']
        fruit_df['price'] = [120, 30, 55, 80, 12]
        fruit_df['quantity'] = [2, 12, 6, 1, 1]
        # print(fruit_df)
        pobj(fruit_df)


class Fruits:

    def get_apple_detail(self, price: float = 0.0):
        result = {
            'fruit': 'Apple',
            'price': price or 120,
            'quantity': 2,
            'available': True
        }
        pobj(result)
        # logger.info(result)
        return result


demo1_obj = Demo1()
demo1_obj.get_int(10)
demo1_obj.get_str('apple')
demo1_obj.get_bool(True)
demo1_obj.get_list([1, 3, 5, 2, 4])
demo1_obj.get_dict({'fruit': 'apple', 'price': 20, 'available': True})
demo1_obj.get_pandas_dataframe()


fruit_obj = Fruits()
fruit_obj.get_apple_detail()
# fruit_obj.get_apple_detail(180)
