# ChangeLog Version


```

---
## v0.0.0 | YYYY-MM-DD
### Feature Name | Action: Added / Update / Fixed | Status: Pending / Hold / Done
- description
- short key points

```


---
## v1.2.3 | 2021-01-31
### Print object with logger | Added | Done
- by using pobjl, User also used the feature of logger, Credit goes to logging author also ;)
- we use the existing [logging](https://docs.python.org/3.8/library/logging.html), We just addon on layer on it

---
## v1.2.2 | 2020-11-30
### Show size in KB | Added | Done
- display size in KB instead of bytes
- basically we convert size of object/variable bytes into KB unit 
- Note: 1000 bytes = 1 KB

---
## v1.2.1 | 2020-11-30
### Also show type & size of object | Added | Done
- additionally we also print type & size of object/variable
- type it's print the which type of variable it's. Like int/str/list/...
- size it's shows the how much memory is occupy by this variable 

---
## v1.1.1 | 2020-11-29
### Print Object | Added | Done
- print your object/variable
- by using simply `pobj` keyword
- like: pobj(your_obj)


---